function parse() {
    var query = document.getElementById('query').value;
    query = query.split(' ');
    if (query.length == 4) {
        if (!(parseInt(query[0], 10) || parseFloat(query[0]))) {
            show_alert();
            return false;
        }
    } else {
        show_alert();
        return false;
    }
    if ((query[1].length != 3) || (query[3].length != 3)) {
        show_alert();
        return false;
    }
    if ((query[2] != 'in') && (query[2] != 'to')) {
        show_alert();
        return false;
    }
    var form = document.forms['convertForm'];
    form.action = '/1.5/'+query[1]+'/to/'+query[3]+'/in'+'/html/';
    document.getElementById('amount').value = query[0];
    form.submit();
    return true;
}


function show_alert() {
    document.getElementById('alert').style.display = 'block';
}