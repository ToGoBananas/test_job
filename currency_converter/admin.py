from django.contrib import admin
from currency_converter.models import Currency


class CurrencyAdmin(admin.ModelAdmin):
    search_fields = ['name']

admin.site.register(Currency, CurrencyAdmin)