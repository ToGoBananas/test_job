from __future__ import absolute_import
from datetime import timedelta
from celery.task import periodic_task
import requests
from currency_converter.models import Currency
from django.core.exceptions import ObjectDoesNotExist

APP_ID = '5f822595357d4324b1af7f6f3c7fe596'
URL = 'http://openexchangerates.org/api/latest.json'


@periodic_task(run_every=timedelta(minutes=60))
def update():
    data = requests.get(URL, params={'app_id': APP_ID}).json()['rates']
    for x in data:
        try:
            a = Currency.objects.get(name=x)
            a.cost = data[x]
            a.save()
        except ObjectDoesNotExist:
            a = Currency(name=x, cost=data[x])
            a.save()
