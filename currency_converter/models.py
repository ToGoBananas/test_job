from django.db import models


class Currency(models.Model):
    name = models.CharField(max_length=3, unique=True)
    cost = models.FloatField()

    def __str__(self):
        return self.name