from django.shortcuts import render
from currency_converter.models import Currency
from django.views.generic import View
from django.views.generic.base import TemplateResponseMixin
from django.http import HttpResponse, JsonResponse, HttpResponseNotFound
from django.core.exceptions import ObjectDoesNotExist
from django.core.cache import cache


def index(request):
    return render(request, 'currency_converter/index.html', {})


class JSONResponseMixin(object):
    def render_to_json_response(self, context, **response_kwargs):
        return JsonResponse(
            self.get_data(context),
            **response_kwargs
        )

    @staticmethod
    def get_data(context):
        try:
            print('Error: ' + context['error'])
        except KeyError:
            context['success'] = 'True'
        else:
            context['success'] = 'False'
        return context


class TextResponseMixin(object):
    def text_response(self, context):
        return HttpResponse(self.get_text_data(context), content_type="text/plain")

    @staticmethod
    def get_text_data(context):
        try:
            print('Error: ' + context['error'])
        except KeyError:
            return context['result']
        else:
            return context['error']


class CurrencyConvertMixedView(View, TemplateResponseMixin, JSONResponseMixin, TextResponseMixin):
    template_name = 'currency_converter/index.html'

    @staticmethod
    def get_context(kwargs, request):
        context = dict()
        try:
            currency1, currency2 = Currency.objects.get(name=kwargs['currency_code_1'].upper()),\
                                   Currency.objects.get(name=kwargs['currency_code_2'].upper())
        except ObjectDoesNotExist:
            context['error'] = "Such currency is absent, check request."
        else:
            name_for_cache = currency1.name + 'to' + currency2.name
            if cache.get(name_for_cache):
                context['result'] = cache.get(name_for_cache)
            else:
                result = currency2.cost/currency1.cost
                context['result'] = result
                cache.set(name_for_cache, result)
            if request.method == 'POST':
                context['query'] = request.POST['query']
                context['result'] = float(request.POST['amount'])*float(context['result'])
        return context

    def post(self, request, *args, **kwargs):
        context = self.get_context(kwargs, request)
        return TemplateResponseMixin.render_to_response(self, context)

    def get(self, request, *args, **kwargs):
        context = self.get_context(kwargs, request)
        if kwargs['format'] == 'json' or self.template_name is None:
            return JSONResponseMixin.render_to_json_response(self, context)
        elif kwargs['format'] == 'html':
            return TemplateResponseMixin.render_to_response(self, context)
        elif kwargs['format'] == 'text':
            return TextResponseMixin.text_response(self, context)
        else:
            return HttpResponseNotFound('<h1>Page not found</h1>')
