from django.conf.urls import url
from currency_converter.views import index, CurrencyConvertMixedView


urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^1\.5/(?P<currency_code_1>[a-zA-Z]{3})/to/(?P<currency_code_2>[a-zA-Z]{3})/in/(?P<format>[a-zA-Z]{4})/$',
        CurrencyConvertMixedView.as_view(), name='currency_converter'),
]